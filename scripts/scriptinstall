#!/usr/bin/env bash
#
# DESC: An installation and deployment script for Patrick's Arch desktop.
#

if [ "$(id -u)" = 0 ]; then
    echo "##################################################################"
    echo "This script MUST NOT be run as root user since it makes changes"
    echo "to the \$HOME directory of the \$USER executing this script."
    echo "The \$HOME directory of the root user is, of course, '/root'."
    echo "We don't want to mess around in there. So run this script as a"
    echo "normal user. You will be asked for a sudo password when necessary."
    echo "##################################################################"
    exit 1
fi

error() { \
    printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

echo "################################################################"
echo "## Syncing the repos and installing 'dialog' if not installed ##"
echo "################################################################"
sudo pacman --noconfirm --needed -Syu dialog || error "Error installing dialog."

lastchance() { \
    dialog --colors --title "\Z7\ZbInstalling Arch!" --msgbox "\Z4WARNING! The installation script is currently in public beta testing. There are almost certainly errors in it; therefore, it is strongly recommended that you not install this on production machines. It is recommended that you try this out in either a virtual machine or on a test machine." 16 60

    dialog --colors --title "\Z7\ZbAre You Sure You Want To Do This?" --yes-label "Begin Installation" --no-label "Exit" --yesno "\Z4Shall we begin installing Arch?" 8 60 || { clear; exit 1; }
}
lastchance || error "User choose to exit."

echo "################################################################"
echo "## Installing packages                                        ##"
echo "################################################################"
sudo pacman --needed --ask 4 -Sy - < pkglist.txt || error "Failed to install required packages"
sudo reflector -c France -a 6 --sort rate --save /etc/pacman.d/mirrorlist || error "Error using reflector"

echo "################################################################"
echo "## Configuring snapper                                        ##"
echo "################################################################"
sudo umount /.snapshots || error "Error unmouting .snapshots"
sudo rm -r /.snapshots || error "Error removing .snapshots"
sudo snapper -c root create-config / || error "Error creating snapper config"
sudo btrfs subvolume delete /.snapshots || error "Error deleting .snapshots subvolume"
sudo mkdir /.snapshots || error "Error recreating .snapshots"
sudo mount -a || error "Error mounting all partitions"
sudo chmod 750 /.snapshots|| error "Error changing access to .snapshots"
sudo cp ../etc/root /etc/snapper/configs || error "Error copying snapper config file"

echo "################################################################"
echo "## Enabling snapper                                           ##"
echo "################################################################"
sudo systemctl enable --now snapper-timeline.timer || error "Error enabling snapper-timeline"
sudo systemctl enable --now snapper-cleanup.timer || error "Error enabling snapper-cleanup"

echo "################################################################"
echo "## Copying config files                                       ##"
echo "################################################################"
# Copy .config folder
sudo cp -r ../.config/ ~ || error "Error copying .config folder"

# Copy backgrounds folder
sudo cp -r ../backgrounds /usr/share/ || error "Error copying backgrounds folder"

# Copy config files
cp ../.xprofile ~ || error "Error copying .xprofile"
cp ../.Xresources ~ || error "Error copying .Xresources"
cp ../.bashrc ~ || error "Error copying .bashrc"
cp ../sabnzbd.ini ~ || error "Error copying sabnzbd.ini"

# Change owner on home directory
sudo chown -R patrick:users ~ || error "Error changing owner on home directory"

# Copy sddm config
sudo cp ../etc/sddm.conf /etc/ || error "Error copying sddm.conf"
sudo cp ../Xsetup /usr/share/sddm/scripts/ || error "Error copying Xsetup"
sudo cp ../theme.conf /usr/share/sddm/themes/elarun/ || error "Error copying sddm theme.conf"

# Activate sound - alsa
amixer sset Master umute || error "Error unmuting Master sound"

# X11 keymap fr
sudo localectl set-x11-keymap fr || error "Error setting X11 FR keymap"

# Set fish as user shell
sudo chsh $USER -s "/bin/fish" || error "Error setting fish as user shell"

# Git global parameters
git config --global user.email "plavenu@hotmail.com"
git config --global user.name "ScottieFF"
git config --global core.editor "vim"

# install AUR yay
cd /opt
sudo git clone https://aur.archlinux.org/yay.git || error "Error git cloning yay"
sudo chown -R patrick:users ./yay || error "Error changing owner on yay directory"
cd yay
makepkg -si || error "Error make package yay"
# yay --noeditmenu --nodiffmenu --save || error "Error configuring yay"

echo "################################################################"
echo "## Installing snapd                                           ##"
echo "################################################################"
yay -S snapd || error "Error installing snapd"
sudo systemctl enable --now snapd.socket || error "Error enabling snapd"
sudo ln -s /var/lib/snapd/snap /snap || error "Error creating symbolic link snapd"

echo "################################################################"
echo "## Installing Brave-browser                                   ##"
echo "################################################################"
yay -S brave-bin || error "Error installing Brave browser"

echo "################################################################"
echo "## Installing JDownloader2                                    ##"
echo "################################################################"
yay -S jdownloader2 || error "Error installing jdownloader2"

echo "################################################################"
echo "## Installing Sabnzbd                                         ##"
echo "################################################################"
yay -S sabnzbd || error "Error installing sabnzbd"
sudo systemctl start sabnzbd@patrick.service || error "Error starting sabnzbd service"

echo "################################################################"
echo "## Installing NordVPN                                         ##"
echo "################################################################"
yay -S nordvpn-bin || error "Error installing nordvpn"
sudo gpasswd -a patrick nordvpn
sudo systemctl enable --now nordvpnd || error "Error enabling nordvpnd service"

echo "################################################################"
echo "## Iinstall AUR colorscript                                   ##"
echo "################################################################"
yay -S shell-color-scripts || error "Error installing shell-color-scripts"

echo "################################################################"
echo "## Install Pycharm                                            ##"
echo "################################################################"
sudo snap install pycharm-community --classic || error "Error installing pycharm"

