#!/bin/bash
dir1=~/saveconfig
dir2=~/savehistory
# Save previous backup 
if [ -d "$dir1" ] 
then
	rm -dr $dir1
fi
if [ ! -d "$dir2" ] 
then
	mkdir $dir2
fi
# Create directories
mkdir -p $dir1/backgrounds
mkdir -p $dir1/themes
# Copy config files
cp ~/.bashrc $dir1
cp -a ~/.config $dir1/
cp -a ~/scripts $dir1/
cp cmd_history $dir1/
cp ~/.dmrc $dir1
cp ~/.sabnzbd.ini/sabnzbd.ini $dir1
#cp /etc/lightdm/lightdm.conf $dir1
#cp /etc/lightdm/lightdm-gtk-greeter.conf $dir1
cp ~/.xprofile $dir1
cp /usr/share/backgrounds/*.* $dir1/backgrounds
cp -r /usr/share/themes/*.* $dir1/themes
# Keep backup of configfiles
timestamp=$(date +%Y-%m-%d_%H-%M-%S)
tar -czf $dir2/configfiles$timestamp.tgz ~/saveconfig
