#!/bin/bash
dir1=~/saveconfig
# 
cd $dir1
cp -a .config/* $dir1/.config
mv save.sh ~
mv restore.sh ~
mv .xprofile ~
mv backgrounds /usr/share/
sudo rm /etc/lightdm/lightdm*.conf
sudo mv lightdm*.conf /etc/lightdm/
mv xmonad.hs ~/.xmonad/
mv bashrc ~
