#
# ##############################################################
# # QTILE Config file - Patrick Lavenu 2022
# ##############################################################
#
import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Drag, Key, Screen, Group, Drag, Click, Rule, KeyChord
from libqtile.command import lazy
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook

#mod4 or mod = super key
mod = "mod4"       # windows  
mod1 = "mod1"      # alt
mod2 = "control"   # control
home = os.path.expanduser('~')
myTerm = "alacritty"


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

keys = [

# Most of our keybindings are in sxhkd file - except these

# SUPER + FUNCTION KEYS
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),

# SUPER + SHIFT KEYS
    Key([mod, "shift"], "q", lazy.window.kill()),
    Key([mod, "shift"], "r", lazy.restart()),

# QTILE LAYOUT KEYS
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),

# CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),

# RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, mod2], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, mod2], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, mod2], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, mod2], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),

# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),

# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),

# TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),

# LAUNCH APPLICATIONS
    Key([mod], "Return", lazy.spawn(myTerm), desc="Launch terminal"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command"),
    Key([mod], "b", lazy.spawn("brave"), desc="Launch Browser"),
    Key([mod, "shift"], "b", lazy.spawn("brave --incognito"), desc="Launch Browser"),
    Key([mod], "d", lazy.spawn("dmenu_run"), desc="Launch DMenu"),
#
    KeyChord([mod], "p", [
        Key([], "e",
            lazy.spawn("./dmscripts/dmconf"),
            desc='Choose a config file to edit'
            )
        ])
    ]
# ##############################################################
# # WORKSPACES DEFINITION
# ##############################################################
groups = []

group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "minus", "egrave", "underscore", "ccedilla", "agrave",]

group_labels = ["", "", "", "", "", "", "", "", "", "",]

group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "floating", "monadtall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

# CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Tab", lazy.screen.next_group()),
        Key([mod, "shift" ], "Tab", lazy.screen.prev_group()),
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        Key(["mod1", "shift"], i.name, lazy.window.togroup(i.name)),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])

# ##############################################################
# # LAYOUTS DEFINITION
# ##############################################################

def init_layout_theme():
    return {"margin":5,
            "border_width":2,
            "border_focus": "#5e81ac",
            "border_normal": "#4c566a"
            }

layout_theme = init_layout_theme()

layouts = [
    layout.MonadTall(margin=8, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    layout.MonadWide(margin=8, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    layout.Matrix(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme)
]

# ##############################################################
# # COLORS FOR THE BAR
# ##############################################################

def init_colors():
    return [["#32D8F0", "#32D8F0"], # color 0: light blue
            ["#2F343F", "#2F343F"], # color 1: dark grey
            ["#c0c5ce", "#c0c5ce"], # color 2: light grey
            ["#fba922", "#fba922"], # color 3: orange
            ["#3384d0", "#3384d0"], # color 4: blue
            ["#f3f4f5", "#f3f4f5"], # color 5: white
            ["#cd1f3f", "#cd1f3f"], # color 6: red
            ["#62FF00", "#62FF00"], # color 7: green fluo
            ["#000000", "#000000"], # color 8: black
            ["#a9a9a9", "#a9a9a9"]] # color 9: grey

# ##############################################################
# # WIDGETS FOR THE BAR
# ##############################################################
colors = init_colors()

def init_widgets_defaults():
    return dict(font="Noto Sans",
                fontsize = 12,
                padding = 2,
                background = colors[1])

widget_defaults = init_widgets_defaults()

def init_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [
               # Display Menu icon
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('jgmenu_run')},
                        foreground=colors[5],
                        padding = 0,
                        fontsize=16
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8],
                        ),
               # Workspaces icons
               widget.GroupBox(font="FontAwesome",
                        fontsize = 16,
                        margin_y = 3,
                        margin_x = 0,
                        padding_y = 6,
                        padding_x = 5,
                        borderwidth = 0,
                        disable_drag = True,
                        active = colors[9],
                        inactive = colors[8],
                        rounded = False,
                        highlight_method = "text",
                        this_current_screen_border = colors[0],
                        foreground = colors[2],
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8],
                        ),
               # Current Layout
               widget.CurrentLayout(
                        font = "Noto Sans Bold",
                        foreground = colors[5],
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8],
                        ),
               # Prompt Mod-r
               widget.Prompt(
                        prompt = prompt,
                        font="Noto Sans",
                        padding = 10,
                        foreground = colors[9]
                        ),
               # Disply current active Window name
               widget.WindowName(font="Noto Sans",
                        fontsize = 14,
                        foreground = colors[5],
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8],
                        ),
               # Display Network usage
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e ls')},
                        foreground=colors[4],
                        padding = 0,
                        fontsize=16
                        ),
               widget.Net(
                        font="FontAwesome",
                        fontsize=12,
                        format = '{down} {up}',
                        interface='enp0s3',
                        foreground=colors[2],
                        padding = 0,
                        prefix = 'k',
                        max_char = 10
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8]
                        ),
               # Display if updates are available from Arch repo 
               widget.CheckUpdates(
                        update_interval = 300,
                        distro = "Arch",
                        display_format = "{updates} Updates",
                        foreground = colors[2],
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')}
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8]
                        ),
               # Display Disk usage
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[4],
                        padding = 0,
                        fontsize=16
                        ),
               widget.DF(
                        partition="/",
                        format = '{p} {uf}{m}b {r:.0f}% ',
                        visible_on_warn = False
                        ),
               widget.DF(
                        partition="/home",
                        format = '{p} {uf}{m}b {r:.0f}% ',
                        visible_on_warn = False
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8],
                        ),
               # Display CPU usage
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[4],
                        padding = 0,
                        fontsize=16
                        ),
               widget.CPU(
                        format = '{load_percent}%',
                        foreground = colors[5]
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8]
                        ),
               # Display Memory usage
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[4],
                        padding = 0,
                        fontsize=16
                        ),
               widget.Memory(
                        font="Noto Sans",
                        format = '{MemUsed:0.2}/{MemTotal:0.3}',
                        measure_mem = 'G',
                        update_interval = 1,
                        fontsize = 12,
                        padding = 0
                        ),
               # Display Sound volume
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[4],
                        padding = 0,
                        fontsize=16
                        ),
               widget.Volume(),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8]
                        ),
               # Display Battery usage
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[4],
                        padding = 0,
                        fontsize=16
                        ),
               widget.Battery(
                        font="Noto Sans",
                        update_interval = 10,
                        format = '{char} {percent:2.0%}',
                        fontsize = 12,
                        foreground = colors[5]
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[8]
                        ),
               # Display Date and Time
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[4],
                        padding = 0,
                        fontsize=16
                        ),
               widget.Clock(
                        foreground = colors[5],
                        fontsize = 12,
                        format="%a %d %B  %H:%M:%S"
                        ),
               # System Tray
               widget.Systray()
              ]
    return widgets_list

widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2

widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26, opacity=0.8)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26, opacity=0.8))]
screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

dgroups_key_binder = None
dgroups_app_rules = []
# #####################################################################################
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# #####################################################################################

@hook.subscribe.client_new
#
def assign_app_group(client):
    d = {}
#     ### Use xprop fo find  the value of WM_CLASS(STRING) -> First field is sufficient ###
    d[group_names[0]] = ["Alacritty", "alacritty" ]
    d[group_names[1]] = [ "brave", "brave-browser", "Brave", "Brave-browser" ]
    d[group_names[2]] = ["code-oss", "Code-oss", "jetbrains-pycharm-ce", "Jetbrains-pycharm-ce" ] 
    d[group_names[3]] = ["Vlc","vlc", "Mpv", "mpv", "Gl", "gl"]
    d[group_names[4]] = ["Gimp", "gimp", "org.inkscape.Inkscape", "Inkscape" ]
    d[group_names[5]] = ["Spotify", "spotify", "Deadbeef", "deadbeef", "Audacity", "audacity" ]
    d[group_names[6]] = ["JDownloader", ]
    d[group_names[7]] = ["Pcmanfm", "Pcmanfm-qt", "pcmanfm", "pcmanfm-qt", ]
    d[group_names[8]] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
              "virtualbox manager", "virtualbox machine", "vmplayer", ]
    d[group_names[9]] = ["Evince", "evince", ]
#
    wm_class = client.window.get_wm_class()[0]
 
    for i in range(len(d)):
        if wm_class in list(d.values())[i]:
            group = list(d.keys())[i]
            client.togroup(group)
            client.group.cmd_toscreen(toggle=False)
# 
# ######################################################################################

main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types
            or window.window.get_name() == "galculator"):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules
])
auto_fullscreen = True
focus_on_window_activation = "smart" # focus or smart

wmname = "LG3D"
