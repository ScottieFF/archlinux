#
# ~/.bashrc
#
export HISTSIZE=5000
export HISTFILESIZE=10000
export HISTCONTROL="ignoreboth:erasedups"
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='exa'
alias ll='exa -la'
alias vi='nvim'
shopt -s histappend
PS1='[\u@\h \W]\$ '
